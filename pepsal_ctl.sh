#!/bin/bash
# # # # # # # # # # # # # # # # # # # # #
#
# Copyright GateHouse 2016
#
# Description:
#   Wrapper for PEPsal.
#   Setting up iptables/ebtables and then runs PEPsal.
#   Cleans up when PEPsal exits (for whatever reason).
#
# Debugging:
#   make -C pepsal/ && bash pepsal_ctl.sh on
#   cat pepsal_ctl.log && tail -f /root/pepsal.log
#
# # # # # # # # # # # # # # # # # # # # #

# Add delay on server -> emulating "Internet delay"
#sudo tc qdisc add dev eno1 root netem delay 200ms   # Adds a delay of 200ms
 
VERSION="1.0"

PEP_QNR="0"
PEP_PORT="5000"

BR_IFACE="br0"

script=$(basename $0)
dir=$(dirname `realpath $0`)

log="$dir/pepsal.log"
script_log="${script%.*}.log"

# Locate binary (Easy for debugging)
pepsal="/usr/local/bin/pepsal"
# TODO: Remove before delivery
[ -f "$dir/src/pepsal" ] && pepsal="$dir/src/pepsal"
[ -f "$dir/pepsal/src/pepsal" ] && pepsal="$dir/pepsal/src/pepsal"

HEAD() { echo -en "\e[1;34m$*\e[0m\n"; echo $* > $log; }
INFO() { echo -en "\e[1;32m$*\e[0m\n"; echo $* > $log; }
ERROR() { echo -en "\e[1;31m$*\e[0m\n"; echo $* > $log; }

Kill_PEPsal() {
    if [ "$(pgrep -x pepsal)" ]; then
        ERROR "Kill old pepsal process..."
        pkill -x pepsal
    fi
}


if [ "$1" = "help" ]; then
    INFO "\n Usage $0 [ Script Option ] [ PEPsal Options ]"
    echo
    echo "Script Options:"
    echo "  test           Test the script is detecting the right IPs"
    echo "  show           Show relevant iptables/ebtables tables"
    echo "  on             Safely startup PEPsal (Kills any running instances)"
    # echo "  on trans       Startup PEPsal in transparent bridging mode"
    echo "  off            Safely killall PEPsal instances"
    echo
    echo "PEPsal Options:"
    echo "  $($pepsal -h 2>&1)"
    echo
    exit

elif [ "$1" = "version" ]; then
    echo "Script ver. $VERSION"
    echo "$($pepsal -V)"
    exit

elif [ "$1" = "off" ]; then
    Kill_PEPsal
    exit

elif [ "$1" = "on" ]; then
    Kill_PEPsal
    sleep .2

    HEAD "Executing PEPsal..."
    INFO "  (Watch script with 'tail -f $script_log')"
    INFO "  (Watch PEPsal output with 'tail -f $log')"
    bash $0 ${@:2} > $script_log 2>&1 &
    exit
fi

# Make sure I'm root
if [ "$(whoami)" != "root" ]; then
    sudo su -c "$0 $@"
    exit
fi

# # # Detecting Internet settings # # #
INET_ROUTE=$(ip route get 8.8.8.8)

INET_IP=$(echo $INET_ROUTE | awk '{print $7}')
INET_IFACE=$(echo $INET_ROUTE | awk '{print $5}')
INET_ADDR=$(ip addr show dev $INET_IFACE | awk "/$INET_IP/ {print \$2}")

# # # Detecting Bridge # # #
BR=$(brctl show $BR_IFACE | awk '{if (NR!=1) print $NF}')
BRIDGE=${BR/$'\n'/ <-> }

if [ "$INET_IFACE" = "$BR_IFACE" ]; then
    PEP_IP=$INET_IP
    PEP_ADDR=$INET_ADDR
    PEP_IFACE=$INET_IFACE
else
    for dev in $BR; do
        if [ "$(ip route show dev $dev)" != "" ]; then
            PEP_IP=$(ip route show dev $dev | awk '/default/ {print $5}')
            PEP_ADDR=$(ip addr show dev $dev | awk "/$PEP_IP/ {print \$2}")
            PEP_IFACE=$dev
            break
        fi
    done
    
    # If not yet found, use bridge IP/ADDR
    if [ -z "$PEP_IP" ]; then
        PEP_IP=$(ip route show dev $BR_IFACE | awk '{print $7; exit}')
        PEP_ADDR=$(ip addr show dev $BR_IFACE | awk "/$PEP_IP/ {print \$2}")
        PEP_IFACE=$BR_IFACE
    fi
fi



EXEC() {
    [ "$1" != "echo" ] && INFO "\$ $@"
    "$@"
}

SH() {
    EXEC $@
    local ret=$?
    if [ $ret -ne 0 ]; then
        ERROR "Failed to exec: $* (Returned $ret)"
        exit $ret
    fi
}


list_rules() {
    # List all rules
    SH iptables -L PREROUTING -vt mangle
    SH iptables -L PREROUTING -vt nat
    SH iptables -L POSTROUTING -vt nat
    SH iptables -L TCP_OPTIMIZATION -vt nat
    SH iptables -L TCP_OPTIMIZATION -vt mangle

    SH ebtables -t broute -L | grep -v '^$'
    
    INFO "Congestion Algorithm"
    sysctl net.ipv4.tcp_congestion_control
}

CLEANUP() {
    HEAD "Flush/create TCP_OPTIMIZATION Chain..."
    iptables -F
    iptables -X
    iptables -t nat -F
    iptables -t nat -X
    iptables -t mangle -F
    iptables -t mangle -X
    iptables -t nat -X TCP_OPTIMIZATION
    iptables -t mangle -X TCP_OPTIMIZATION

    SH iptables -P INPUT ACCEPT
}

echo ""
HEAD "--- Detected PEP Setup ---"
INFO " $($pepsal -V) ($pepsal)"
INFO " Bridge:         $(printf '%-10s (%s)' $BR_IFACE $BRIDGE)"
INFO " Space (Client): $(printf '%-10s %s (%s)' $PEP_IFACE $PEP_IP $PEP_ADDR)"
INFO " Internet:       $(printf '%-10s %s (%s)' $INET_IFACE $INET_IP $INET_ADDR)"
echo ""

[ "$1" = "test" ] && exit
[ "$1" = "show" ] && list_rules && exit

Kill_PEPsal
CLEANUP

# Forwarding packages
#sysctl net.ipv4.ip_forward=1
echo 1 > /proc/sys/net/ipv4/ip_forward

[ "$1" = "off" ] && HEAD " <OFF> " && exit 0

#echo "TCP Tuning"
# modprobe tcp_hybla tcp_bic tcp_westwood
# echo 108544 > /proc/sys/net/core/wmem_max 
# echo 108544 > /proc/sys/net/core/rmem_max 
# echo "4096 87380 4194304" > /proc/sys/net/ipv4/tcp_rmem
# echo "4096 16384 4194304" > /proc/sys/net/ipv4/tcp_wmem

#echo "8192 2100000 8400000" >/proc/sys/net/ipv4/tcp_mem
#echo "8192 2100000 8400000" >/proc/sys/net/ipv4/tcp_rmem
#echo "8192 2100000 8400000" >/proc/sys/net/ipv4/tcp_wmem

HEAD "Masquerade my Address..."
SH iptables -t nat -A POSTROUTING -j MASQUERADE

if [[ "$1" =~ "trans" ]]; then
    shift
    # INFO: This process crashes:
    #       /usr/bin/node /root/gxsipe/nodeserver/bin/www

    if [ "$(pgrep dhcpd)" != "" ]; then
        HEAD "Killing local DHCP server & client..."
        # SH systemctl stop netctrl
        # SH systemctl --fail stop netctrl@$PEP_IFACE
        SH systemctl stop dhcpd4@$BR_IFACE
        SH systemctl stop dhcpcd@$PEP_IFACE
        SH systemctl stop dhcpcd@$INET_IFACE
        
        # Notes:
        #       dhcpcd --dumplease enp0s20f0
        #       cat /var/lib/dhcp/dhcpd.leases
    fi
    
    PEP_IFACE=enp0s20f1
    INET_IFACE=enp0s20f0
    
    # TODO: Add interface to bridge instead of resetting the whole thing.
    
    if [ "$BRIDGE" != "$INET_IFACE <-> $PEP_IFACE" ]; then
        # TODO: systemctl status netctl@enp0s20f0.service
        HEAD "Delete old bridge ($BRIDGE)..."
        EXEC ip link set dev $BR_IFACE down
        EXEC brctl delbr $BR_IFACE

        HEAD "Setting up new bridge ($PEP_IFACE <-> $INET_IFACE)..."
        EXEC ip addr flush $PEP_IFACE
        EXEC ip addr flush $INET_IFACE
        SH ip link set dev $PEP_IFACE promisc on
        SH ip link set dev $INET_IFACE promisc on
        SH brctl addbr $BR_IFACE
        SH brctl addif $BR_IFACE $PEP_IFACE $INET_IFACE

        HEAD "Getting a new IP address..."
        SH systemctl start dhcpcd@$BR_IFACE
        INET_IP=$(systemctl status dhcpcd@br0.service | awk '/leased/ {print $8; exit}')
        INET_ADDR=$(systemctl status dhcpcd@br0.service | awk '/route to/ {print $10; exit}')
        INFO "  => $BR_IFACE ($INET_IP) ($INET_ADDR)\n"
    fi

    HEAD "Setting up Transparent TCP Proxy..."
    HEAD " - Redirect All TCP traffic to port $PEP_PORT..."
    SH ebtables -t broute -A BROUTING -i $BR_IFACE -p IPv4 --ip-proto TCP -j redirect --redirect-target ACCEPT
    SH ebtables -t broute -A BROUTING -i $BR_IFACE -p IPv6 --ip6-proto TCP -j redirect --redirect-target ACCEPT

    HEAD " - Filter SYNs from NetFilter Queue Nr$PEP_QNR..."
    SH iptables -t mangle -A PREROUTING ! -d $INET_IP -p tcp --syn -j NFQUEUE --queue-num=$PEP_QNR

    HEAD " - Redirect all TCP to port $PEP_PORT..."
    SH iptables -t nat -A PREROUTING ! -d $INET_IP -p tcp -j REDIRECT --to-port $PEP_PORT
    
    # HEAD " - Imposes as client (192.168.0.124)"
    #  -i $BR_IFACE
    # SH iptables -t nat -D POSTROUTING -p tcp -s 192.168.0.102 -j SNAT --to-source 192.168.0.124
    # SH iptables -t nat -D PREROUTING -p tcp -d 192.168.0.124 -j DNAT --to-destination 192.168.0.102
    
# Chain for checking against multiple destinations
#iptables -t nat -F CHECK_TCP
#iptables -t nat -A CHECK_TCP ! -i br0 -j RETURN
#iptables -t nat -A CHECK_TCP -d 172.27.0.1 -j RETURN
#iptables -t nat -A CHECK_TCP -d 192.168.0.102 -j RETURN
#iptables -t nat -A CHECK_TCP -p tcp -j REDIRECT --to-port 5000
#iptables -I PREROUTING -t nat -p tcp -j CHECK_TCP

#iptables -t mangle -F CHECK_TCP
#iptables -t mangle -A CHECK_TCP ! -i br0 -j RETURN
#iptables -t mangle -A CHECK_TCP -d 172.27.0.1 -j RETURN
#iptables -t mangle -A CHECK_TCP -d 192.168.0.102 -j RETURN
#iptables -t mangle -A CHECK_TCP -p tcp --syn -j NFQUEUE --queue-num=0
#iptables -I PREROUTING -t mangle -p tcp --syn -j CHECK_TCP

#pepsal -v

else
    HEAD "Setting up NAT TCP Proxy..."
    # TODO: Remove TCP_OPTIMIZATION
    iptables -t nat -N TCP_OPTIMIZATION
    iptables -t mangle -N TCP_OPTIMIZATION

    HEAD "Filter SYNs from NetFilter Queue Nr$PEP_QNR..."
    SH iptables -I PREROUTING -t nat -p tcp --syn -j TCP_OPTIMIZATION
    SH iptables -I PREROUTING -t mangle -p tcp --syn -j TCP_OPTIMIZATION
    SH iptables -t mangle -I TCP_OPTIMIZATION -i $BR_IFACE -s $PEP_ADDR -p tcp -j NFQUEUE --queue-num=$PEP_QNR
    SH iptables -t mangle -I TCP_OPTIMIZATION -i $INET_IFACE -d $PEP_ADDR -p tcp -j NFQUEUE --queue-num=$PEP_QNR

    HEAD "Redirect all TCP behind the NAT to port $PEP_PORT..."
    SH iptables -t nat -I TCP_OPTIMIZATION -i $BR_IFACE -s $PEP_ADDR -p tcp -j REDIRECT --to-port $PEP_PORT
    SH iptables -t nat -I TCP_OPTIMIZATION -i $INET_IFACE -d $PEP_ADDR -p tcp -j REDIRECT --to-port $PEP_PORT
fi

# Execute PEPsal (Cleanup on exit)
function cleanup() {
    INFO "Cleanup..."
    CLEANUP
    HEAD "Masquerade my Address..."
    SH iptables -t nat -A POSTROUTING -j MASQUERADE
    # TODO: iptables-restore /path/to/gxsipe/iptables.dat
}
trap cleanup EXIT

HEAD "Executing PEPsal..."
# TODO: If -a in ${@:2}, replace $INET_IP
INFO "\$ $pepsal -v -a $INET_IP ${@:2} > $log 2>&1 &"
INFO "  (Watch with 'tail -f $log')"
$pepsal -v -a $INET_IP ${@:2} > $log 2>&1

