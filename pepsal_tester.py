#!/usr/bin/env python

""" Throughput testing for the GateHouse GLE

    Copyright (C) 2016 GateHouse A/S
"""

import os
import re
import sys
import time
import glob
import serial
import getpass
import argparse
import subprocess

from serial.tools.list_ports import comports

PEPSAL_SCRIPT = 'pepsal_ctl.sh'


if sys.platform == 'win32':
    timestamp = time.clock
else:
    timestamp = time.time


class PEPsalError(Exception):
    pass


def header(msg):
    print("\033[1;34m{:s}\033[0m".format(msg))


def info(msg):
    print("\033[1;32m{:s}\033[0m".format(msg))


def success(msg):
    print("\033[1;82m{:s}\033[0m".format(msg))


def error(msg):
    print("\033[1;31m{:s}\033[0m".format(msg))


def warning(msg):
    print("\033[1;33m{:s}\033[0m".format(msg))


def cmd2str(args):
    if isinstance(args, str):
        return args
    command = ''
    for a in args:
        command += ' "{}"'.format(a) if ' ' in a else ' '+a
    return command


def shell(command, verbose=1, exception=False):
    """ Execute command

    Verbosities:
        1: Prints the command
        2: Prints the command & its output
        0: Suppresses all output, including errors

    :param command: Shell command to execute
    :param verbose: Verbosity (Default True)
    :param exception: Throw an exception on failure (Default False)

    :return: Tuple with exit code & output string
    """
    try:
        cmd = cmd2str(command)
        if verbose > 0:
            info("$ {:s}".format(cmd))
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        if verbose > 1:
            print(output)
        return 0, output
    except (OSError, subprocess.CalledProcessError) as err:
        if exception or verbose > 1:
            print(err.output)
            error("Error {:d}: {:s}".format(err.returncode, err))
        if exception:
            raise PEPsalError(err)
        return err.returncode, err.output

# TODO: def async(command, verbose=True)


def loop_command(command, loops=1, wait=3, verbose=1, result_cb=None):
    """ Execute a command X times & wait some seconds in-between

    :param command: Command to execute
    :param loops: How many loops
    :param wait: How long to wait in-between the loops
    :param verbose: Parses verbosity to commands
    """
    cmd = cmd2str(command)
    if verbose:
        info("Loop {:d}x~$ {:s}".format(loops, cmd))

    for i in range(loops):
        ret, output = shell(cmd, None, exception=True)
        if result_cb:
            result_cb(ret, output)
        if i != (loops-1):
            time.sleep(wait)


iperf_results = []
def iperf_result(ret, output):
    """ Parse the output if iperf3 & print the result
    """
    global iperf_results
    kbps = {'K': 1.0, 'M': 1024.0, 'G': 1024.0*1024.0}
    for line in reversed(output.split('\n')):
        if 'receiver' in line:
            # Remembers result for further handling
            m = re.search(' 0.00-\d+.\d+ +sec +(\d+.\d+ +[KMG]?)Bytes +'
                          '(\d+.\d+ +[KMG]?)bits/sec.*', line)
            if m:
                bw = m.group(2)
                number, unit = bw.split()
                iperf_results.append(float(number) * kbps[unit])
                print(line.replace('            receiver', '('+bw+'bps)'))
            else:
                print(line)
            break


def run_iperf(command, args, txt=''):
    header("Loop {:s} {:d}x~$ {:s}".format(txt, args.loops, command))
    loop_command(command, args.loops, verbose=False, result_cb=iperf_result)
    test = iperf_results[-3:]
    info('  Average BW: {:.2f} Kbps'.format(sum(test) / len(test)))


def iperf_show_results(loops, base_bw=0.0):
    """ Show all the results found via iperf_result()
    """
    avg_bw = []
    global iperf_results
    for i in range(0, len(iperf_results), loops):
        test = iperf_results[i:i+loops]
        avg_bw.append(sum(test) / len(test))

    if base_bw:
        max_bw = max(avg_bw+[base_bw])
        min_bw = min(avg_bw+[base_bw])
    else:
        max_bw = max(avg_bw)
        min_bw = min(avg_bw)

    # If MIN Vs MAX is less than 1% difference we neglect the result
    neglectable = bool(100*max_bw/min_bw-100 < 1.0)
    # max_index = next(i for i, bw in enumerate(avg_bw) if bw == max_bw)
    # min_index = next(i for i, bw in enumerate(avg_bw) if bw == min_bw)
    for i, bw in enumerate(avg_bw):
        more_bw = 100*bw/min_bw-100
        if bw == max_bw and base_bw:
            success('  Test {:d} Average BW: {:.2f} ({:.2f}% better)'.format(i, bw, more_bw))
        elif neglectable or more_bw:
            info('  Test {:d} Average BW: {:.2f} ({:.2f}% better)'.format(i, bw, more_bw))
        else:
            warning('  Test {:d} Average BW: {:.2f} (worst)'.format(i, bw))

    iperf_results = []
    return max_bw


# # # # # # # # # # # # # #
# GLE Serial commnication #
# # # # # # # # # # # # # #

def serial_ports(search=None):
    """ Lists serial port names
    """
    result = []
    for port, txt, hwinfo in comports():
        if not search or search in txt+hwinfo:
            result.append(port)
    return result


class GLE(object):
    """ Serial connection to the GLE
    """
    def __init__(self, port, user='root', passwd=None,
                 script=PEPSAL_SCRIPT, verbose=True, **kwargs):
        super(GLE, self).__init__()
        self.port = port
        self.user = user
        self.passwd = passwd
        self.verbose = verbose
        self.console = '[{:s}@GLE ~]# '.format(user)
        self.re_console = re.compile(r'\[\w+@\w+\s+~\]#')
        self.pepsal_ctl = script
        try:
            kwargs.setdefault('timeout', 0.5)
            kwargs.setdefault('baudrate', 115200)

            self.s = serial.Serial(port, **kwargs)
            self.s.flushInput()
            self.s.flushOutput()
            self.read = self.s.read
            self.write = self.s.write

            self.verbose = False
            self.login(user, passwd)
            self.verbose = verbose
        except (OSError, serial.SerialException) as err:
            raise PEPsalError(err)

    def close(self):
        self.s.close()

    def log(self, msg, force=False):
        if force or self.verbose:
            print(msg)

    def send(self, msg, verbose=True):
        """ Send a command & return the echo
        """
        if verbose and self.verbose:
            print('{:s} < {:s}'.format(self.port, msg))
        self.s.write(bytes(msg+'\n'))
        return self.readline(timeout=1)

    def wait_for_console(self, timeout=120):
        """ Wait up to 2min for a console
        """
        try:
            return list(self.readlines(timeout=timeout))
        except StopIteration:
            raise PEPsalError("Never got a console :'(")

    def readline(self, timeout=None, delim='\n'):
        """ Read characters until timeout or a newline character is found
        """
        line = []
        start = timestamp()
        while True:
            c = self.read()
            if c == delim:
                break
            if c == '' and timeout and timestamp() - start > timeout:
                break
            line.append(c)
        # if log:
        #     self.log('{:s} > {:s}'.format(self.port, line), force=True)

        # TODO: Remove rstrip()!
        return ''.join(line).rstrip()

    def readlines(self, timeout=3, until=None):
        """ Read lines until timeout or until a regex is found (Default console)
        """
        start = timestamp()
        while True:
            line = self.readline(min(1, timeout)).strip()
            timeout -= timestamp() - start
            if (line == '' and timeout <= 0) or (until and re.match(until, line)):
                # print('Done (Timeleft: %f) (%s)' % (timeout, line))
                break
            if re.match(self.re_console, line):
                # print('Console! (%s)' % (line))
                break
            yield line

    def login(self, user=None, passwd=None):
        """ Login to the GLE

        :return: True or throws an exception if login is incorrect
        """
        timeouts = 0
        sent_user = False
        if user:
            self.user = user
        if passwd:
            self.passwd = passwd
        self.s.write(bytes('\n'))
        while True:
            line = self.readline(timeout=1).lstrip()
            # self.log('Login: "%s"' % line)
            if not line:
                timeouts += 1
                if timeouts > 5:
                    timeouts = 0
                    self.s.write(bytes("\003"))
                    self.s.read()
            if line.endswith('login:'):
                self.send(self.user, verbose)
                sent_user = True
            elif line.startswith('Password:'):
                if not self.passwd:
                    self.passwd = getpass.getpass('Please enter GLE password:')
                self.send(self.passwd, verbose=False)
                timeouts = 0
            elif line.startswith('['+self.user+'@'):
                self.console = line+' '
                self.re_console = re.escape(line)
                break
            elif 'Login incorrect' in line and sent_user:
                raise PEPsalError(line + ' ('+self.user+':'+passwd+')')

        # Locate the PEPSAL_SCRIPT
        self.send('ls ' + self.pepsal_ctl, verbose=False)
        line = self.readline(timeout=1)
        # TODO: Upgrade if modified is different than mine
        if 'cannot access' in line:
            # TODO: "Copy" PEPsal_ctr.sh script with (echo "" > PEPsal_ctr.sh)
            self.wait_for_console()
            self.send('find . -iname ' + self.pepsal_ctl, verbose=False)
            try:
                line = self.readlines(15, self.pepsal_ctl).next()
            except StopIteration:
                raise PEPsalError('Could not find ' + self.pepsal_ctl)

        # self.log("Found at %s" % line)
        self.pepsal_ctl = line
        return True

    def reboot(self):
        """ Reboot the GLE
        """
        self.send('reboot')
        for line in self.readlines(timeout=1000):
            if line:
                print(line)
        self.login(user=self.user, passwd=self.passwd)

    def execute(self, cmd, timeout=1000):
        """ Execute a command on the GLE
        """
        try:
            self.verbose = False
            print(self.console + cmd)
            self.wait_for_console(timeout=1)
            if cmd.startswith('reboot'):
                self.reboot()
            else:
                self.send(cmd)
            if cmd.startswith('exit') or cmd.startswith('quit'):
                raise KeyboardInterrupt()
            for line in self.readlines(timeout=timeout):
                print(line)
        except KeyboardInterrupt:
            print('Byebye')

    def shell(self, timeout=240):
        """ Go into shell mode (Blocks until user exits)
        """
        try:
            self.verbose = False
            get_input = input if sys.version_info[0] >= 3 else raw_input
            while True:
                cmd = get_input(self.console)
                if cmd.startswith('exit') or cmd.startswith('quit'):
                    raise KeyboardInterrupt()

                if cmd.startswith('reboot'):
                    self.reboot()
                else:
                    self.send(cmd)

                for line in self.readlines(timeout=timeout):
                    print(line)
        except KeyboardInterrupt:
            print('Byebye')

    def pep_ctl(self, cmd, verbose=False):
        self.wait_for_console(timeout=1)
        self.send('bash {:s} {:s}'.format(self.pepsal_ctl, cmd), verbose)
        if not verbose:
            self.wait_for_console(timeout=5)
        else:
            for line in self.readlines(timeout=5):
                self.log('{:s} > {:s}'.format(self.port, line))

    def pep_test(self, verbose=True):
        self.pep_ctl('test', verbose)

    def pep_show(self, verbose=True):
        self.pep_ctl('show', verbose)

    def pep_on(self, verbose=False):
        # header('- PEPsal ON - ')
        self.pep_ctl('on', verbose)

    def pep_off(self, verbose=False):
        # header('- PEPsal OFF - ')
        self.pep_ctl('off', verbose)


if __name__ == "__main__":
    gle = None
    log = 'log'
    verbose = 2
    server = '131.165.109.69'
    url = 'http://{:s}/gatehouse.dk'.format(server)
    wget_args = '--page-requisites -e robots=off'
    iperf_args = '-k -i0 -t{time} -P{parallel}'

    iperf_args += ' -p80'

    try:
        gle_port = serial_ports('CP2104')[0]
    except IndexError:
        gle_port = ''
        warning(" GLE serial port was not found... Plugin serial to USB cable.")

    exe = '.exe' if os.name == 'nt' else ''
    wget = 'wget' + exe
    iperf = 'iperf3' + exe

    parser = argparse.ArgumentParser()
    parser.add_argument('-w', '--wget', action='store_true', help='Run wget test')
    parser.add_argument('-i', '--iperf', action='store_true', help='Run iperf test')
    parser.add_argument('-x', '--exec-dir', type=str, help='Path to executables')
    parser.add_argument('-l', '--log', default=log, help='Path to log directory')

    parser.add_argument('-s', '--serial', default=gle_port, help='GLE serial port')
    parser.add_argument('--user', type=str, default='root', help='Set GLE username (Default root)')
    parser.add_argument('--passwd', type=str, default='', help='Set GLE password (Use "none" for empty)')
    parser.add_argument('--cmd', nargs='?', const=True, default=False, help='Drop into GLE serial command prompt')

    parser.add_argument('-a', '--address', default=server, help='Remote server address')
    parser.add_argument('--wget-url', default=url, help='URL to get with Wget')
    parser.add_argument('--wget-args', default=wget_args, help='Arguments for Wget')
    parser.add_argument('--iperf-args', default=iperf_args, help='Arguments for Iperf3')

    # parser.add_argument('--parallel', type=int, default=3, help='Amount of parallel commands to run')
    parser.add_argument('--loops', type=int, default=3, help='Amount of loops to run the command')
    args, argv = parser.parse_known_args()

    # Drop to Serial port Command line?
    if args.serial:
        gle = GLE(args.serial, user=args.user, passwd=args.passwd)
        if args.cmd:
            gle.verbose = False
            if isinstance(args.cmd, str):
                gle.execute(args.cmd)
            else:
                header('--- GLE Command Prompt ---')
                gle.shell()
            sys.exit()
    elif args.cmd:
        error('\n Command prompt not possible: No GLE detected. \n')
        sys.exit(1)

    args.wget = True
    args.iperf = True

    if args.exec_dir:
        wget = os.path.join(args.exec_dir, wget)
        iperf = os.path.join(args.exec_dir, iperf)

    header("\n --- PEPsal GLE Tester --- \n")
    info("   Loops:    {:d}".format(args.loops))
    # info("   Parallel: {:d}".format(args.parallel))

    if args.wget:
        ret, version = shell(wget + ' --version', 0)
        if ret != 0:
            raise PEPsalError(wget + ' not found')
        info("   Wget:     {:s}".format(version.split('\n')[0]))
        wget_command = "{:s} {:s} {:s}".format(wget, args.wget_args, args.wget_url)
        info("{:13s}$ {:s}".format('', wget_command))

    if args.iperf:
        ret, version = shell(iperf + ' --version', 0)
        if ret:
            raise PEPsalError(iperf + ' not found')
        info("   Iperf:    {:s}".format(version.split('\n')[1]))
        iperf_command = "{:s} -c {:s} {:s}".format(iperf, args.address, args.iperf_args)
        info("{:13s}$ {:s}".format('', iperf_command))

    if args.serial:
        info("   GLE:     {:s}".format(args.serial))
        gle.pep_off()

    # TODO: Server SSH connection - setup?
    # traceroute $server | grep "gx.test"
    #   sudo ip route add $server via $gle_gw

    print('')

    if not os.path.isdir(args.log):
        os.makedirs(args.log)

    if gle:
        gle.pep_test()
        print('')

    start = timestamp()
    gle.pep_off(verbose=True)

    header('\n--- Testsuite with PEP OFF (x{:d}) ---'.format(args.loops))
    iperf_dl_cmd = iperf_command + ' -R'
    run_iperf(iperf_dl_cmd.format(time=10, parallel=1), args, txt="DownloadStream")
    run_iperf(iperf_dl_cmd.format(time=120, parallel=1), args, txt="DownloadStable")
    run_iperf(iperf_dl_cmd.format(time=10, parallel=10), args, txt="10xParallel")
    ### run_iperf(iperf_dl_cmd.format(time=120, parallel=10), args, txt="10xStable")
    run_iperf(iperf_command.format(time=10, parallel=1), args, txt="UploadStream")
    run_iperf(iperf_command.format(time=120, parallel=1), args, txt="UploadStable")

    if gle:
        gle.pep_on(verbose=True)
        header('\n--- Testsuite with PEP ON (x{:d}) ---'.format(args.loops))
        run_iperf(iperf_dl_cmd.format(time=10, parallel=1), args, txt="DownloadStream")
        run_iperf(iperf_dl_cmd.format(time=120, parallel=1), args, txt="DownloadStable")
        run_iperf(iperf_dl_cmd.format(time=10, parallel=10), args, txt="10xParallel")
        run_iperf(iperf_command.format(time=10, parallel=1), args, txt="UploadStream")
        run_iperf(iperf_command.format(time=120, parallel=1), args, txt="UploadStable")
        gle.pep_off()

    success("\nAll testing done")
    duration = timestamp() - start
    ftime = '%H:%M:%S hr' if duration > 60*60 else '%M:%S min'
    print(time.strftime('Test time: {:s}'.format(ftime), time.gmtime(duration)))
    """
    iperf_results.append(875)
    iperf_results.append(875)
    iperf_results.append(871)
    iperf_results.append(866)
    iperf_results.append(863)
    iperf_results.append(865)
    iperf_show_results(args.loops)
    """
    sys.exit()
    """
    # 1. Run all tests
    all_results = []
    for l in range(args.loops):
        results = []
        commands = []
        sys.stdout.write("\r Running Iteration {:d}".format(l+1))
        sys.stdout.flush()
        for i in range(args.parallel):
            cmd = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
            cmd.i = i+1
            commands.append(cmd)

        for cmd in commands:
            (err, out) = cmd.communicate()
            if cmd.returncode:
                print("ERROR for iteration {}, command {}:\n".format(l, cmd.i))
                print(" Error ({}): {}\n [{}]".format(cmd.returncode, err, out))
            else:
                for line in reversed(out.split('\n')):
                    if line.startswith('Downloaded:'):
                        results.append(line)
                        break
        all_results.append(results)
    print("\n Finished all iterations\n")

    # 2. Parse the results
    print("Results:")
    grand_avg_time = grand_total_time = 0.0
    grand_avg_bit_rate = grand_total_bit_rate = 0.0
    for i, interation in enumerate(all_results, start=1):
        avg_time = total_time = 0.0
        avg_bit_rate = total_bit_rate = 0.0
        print(" Iteration {:d}".format(i))
        for n, result in enumerate(interation, start=1):
            print("   {:d}) {:s}".format(n, result))

            # Extracting time & bit rate from result line
            m = re.search('Downloaded: (\d+) files, (\d+.?\d*.?\w+) in (\d+.?\d*\w+) \((\d+.?\d* \w+/\w+)\)', result)
            if m:
                files, nbytes, seconds, bit_rate = m.groups()
                time_match = re.match('(\d+.?\d*)(.?\w+)', seconds)
                if time_match:
                    avg_time += 1
                    total_time += float(time_match.group(1))

                bit_rate_match = re.match('(\d+\.?\d*) ?(\w+)/s', bit_rate)
                if bit_rate_match:
                    unit = bit_rate_match.group(2)
                    avg_bit_rate += 1
                    total_bit_rate += float(bit_rate_match.group(1))

        grand_avg_time += avg_time
        grand_total_time += total_time
        grand_avg_bit_rate += avg_bit_rate
        grand_total_bit_rate += total_bit_rate

        if avg_time and avg_bit_rate:
            avg_time = total_time/avg_time
            avg_bit_rate = total_bit_rate/avg_bit_rate
            print("   Average: {:.2f}s ({:.2f} {:s})"
                  .format(avg_time, avg_bit_rate, unit))

    if grand_avg_time and grand_avg_bit_rate:
        grand_avg_time = grand_total_time/grand_avg_time
        grand_avg_bit_rate = grand_total_bit_rate/grand_avg_bit_rate
        print("\n Grand Average: {:.2f}s ({:.2f} {:s})"
              .format(grand_avg_time, grand_avg_bit_rate, unit))
    """

