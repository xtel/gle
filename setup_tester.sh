#!/bin/bash
#
# Description:
#	Routes all SSH through Wifi connection & the rest through GLE
#
# Remove rules:
#	sudo iptables -F -t nat && sudo iptables -F -t mangle

SH() { echo "\$ $@"; "$@"; }

if [ "$(whoami)" != "root" ]; then
	sudo su -c "$0 $@"
	exit
fi

SERVER=131.165.109.69
# TODO: if [ $1 is IP ];
GLE=$(dig +short gx.test)
ETH=$(ip route get 8.8.8.8)
ETH_DEV=$(echo $ETH | awk '{print $5}')
ETH_IP=$(echo $ETH | awk '{print $7}')
WIFI=$(iwconfig 2> /dev/null | grep IEEE | awk '{print $1}')
WIFI_IP=$(ip addr show dev $WIFI | awk '/inet / {print $2;exit}' | cut -f1 -d/)
WIFI_ROUTE=$(ip route show default dev $WIFI | awk '/default/ {print $3;exit}')

# Cleanup
iptables -F -t nat
iptables -F -t mangle
ip route flush table wifi-route
ip rule del table wifi-route 2> /dev/null
[ "$1" == "off" ] && exit

# Turning OFF Spoof protection
# sysctl net.ipv4.conf.$WIFI.rp_filter=0
for f in /proc/sys/net/ipv4/conf/*/rp_filter; do
	echo 0 > $f
done
echo 1 > /proc/sys/net/ipv4/route/flush


# Adding new routing table
if [ ! "$(grep wifi-route /etc/iproute2/rt_tables)" ]; then
	echo 200 wifi-route >> /etc/iproute2/rt_tables
fi

# Setting up routes
SH ip route add $SERVER via $GLE dev $ETH_DEV
SH ip route add $SERVER via $WIFI_ROUTE dev $WIFI table wifi-route
SH ip rule add fwmark 0x1 table wifi-route

# Mark SSH connection for ssh-route
SH iptables -A OUTPUT -t mangle -o $ETH_DEV -p tcp --dport 22 -j MARK --set-mark 1
SH iptables -A POSTROUTING -t nat -o $WIFI -p tcp --dport 22 -j SNAT --to $WIFI_IP
# SH iptables -A PREROUTING -t nat -d $WIFI_IP -p tcp --sport 22 -j DNAT --to-destination $ETH_IP

